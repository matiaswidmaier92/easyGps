"use strict";

module.exports = function(sequelize, DataTypes) {
  const User = sequelize.define("User", {
    name: DataTypes.STRING,
    lastname: DataTypes.STRING,
    password: DataTypes.STRING,
    username: DataTypes.STRING,
    role: DataTypes.STRING
  }/*, {
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Task)
      }
    }
  }*/);
  User.sync({force: false});

  return User;
};
