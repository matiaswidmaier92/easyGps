"use strict";

module.exports = function(sequelize, DataTypes) {
  var GpsPoint = sequelize.define("GpsPoint", {
    trackerId: DataTypes.BIGINT,
    date: DataTypes.DATE,
    point: DataTypes.GEOMETRY,
    speed: DataTypes.INTEGER,
    heading: DataTypes.STRING,
    altitude: DataTypes.INTEGER,
    ioStatus: DataTypes.BIGINT,
    gpsValid: DataTypes.BOOLEAN
  }/*, {
    classMethods: {
      associate: function(models) {
        GpsPoint.hasMany(models.Task)
      }
    }
  }*/);
  GpsPoint.sync({force: false});

  return GpsPoint;
};
