const express = require('express');
const router = express.Router();
const model  = require('../models');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const fs        = require("fs");
const path      = require("path");
const config = require(path.join(__dirname, '..', 'config', 'config.json'));

/* GET users listing. */
router.get('/', function(req, res, next) {
  model.User.findAll(
    {attributes: ['username','name','lastname','role']})
    .then(function(users) {
      res.json(users);
  })
  .catch(function(err){
    console.log(err);
  });
});

router.put('/', function(req, res, next) {
  let user = {
    id: req.body.id,
    name: req.body.name,
    lastname: req.body.lastname,
    password: bcrypt.hashSync(req.body.password, saltRounds),
    username: req.body.username,
    role: req.body.role
  }
  let id = user.id;
  delete user.id;
  model.User.update(user, {where:{id:id}})
  .then((data) => {
    console.log('User updated ', data.dataValues);
    return res.json(data.dataValues);
  });
});

router.post('/', function(req, res, next) {
  let user = {name: req.body.name,
    lastname: req.body.lastname,
    password: bcrypt.hashSync(req.body.password, saltRounds),
    username: req.body.username,
    role: req.body.role
  }
  model.User.create(user)
  .then((data) => {
    console.log('User created ', data.dataValues);
    return res.json(data.dataValues);
  });

});

router.post('/isAuthenticated', function (req, res, next){
  /*return res.json(true);*/
  let token = req.body.token;
  jwt.verify(token, config.token, function(err, decoded) {
    let sessionControl = {
      isAuthenticated: false,
      decoded: null,
      error: null
    };
    if(err)
      sessionControl.error = err;
    if(decoded){
      sessionControl.isAuthenticated = true;
      sessionControl.decoded = decoded;
    }
    return res.json(sessionControl);
  });
});

router.post('/singin', function (req, res, next){
  model.User.findOne({where: {username: req.body.username}})
  .then((data) => {
    if (data.dataValues.username != req.body.username || !bcrypt.compareSync(req.body.password, data.dataValues.password)){
      return res.status(401).json({
        title: 'Login failed',
        error: {message: 'Invalid login credentials'}
      });
    }
    var token = jwt.sign({user: data.dataValues}, config.token, {expiresIn: 9999999999});
    res.status(200).json({
        message: 'Successfully logged in',
        token: token,
        userId: data.dataValues.id
    });
  });
});

module.exports = router;
