const models  = require('../models');
const express = require('express');
const router  = express.Router();


router.get('/', (req, res) => {
	let params = req.query;
	models.sequelize.query(`SELECT "id", "trackerId", "date", "point", "speed", "heading", "altitude", "ioStatus", "gpsValid", "createdAt", "updatedAt" 
	FROM "GpsPoints" AS "GpsPoint" 
	WHERE "GpsPoint"."trackerId" = '` + params.trackerId + `' 
	AND ("GpsPoint"."date" >= '` + new Date(parseInt(params.startDate)).toISOString() + `' 
	AND "GpsPoint"."date" <= '` + new Date(parseInt(params.endDate)).toISOString() + `') 
	AND ("GpsPoint"."speed" >= '` + params.startSpeed + `' AND "GpsPoint"."speed" <= '` + params.endSpeed + `')
	AND ST_Intersects("GpsPoint"."point"::geometry, ST_MakeEnvelope(9.855459, -87.129975, -52.419719, -31.363149, 4326));`,
	{type: models.sequelize.QueryTypes.SELECT})
	.then((rows) => {
		res.json(rows);
	}).catch((err) => {
		console.log('error al listar puntos', err);
	})
});

router.get('/segments', (req, res) => {
	let params = req.query
	/*models.GpsPoint.findAll({
		where: {
			trackerId: params.trackerId,
			date: {
				$gte: new Date(parseInt(params.startDate)).toISOString(),
				$lte:	new Date(parseInt(params.endDate)).toISOString()
			},
			speed: {
				$gte: params.startSpeed,
				$lte:	params.endSpeed
			}
		}
	})*/
	models.GpsPoint.query(
		"SELECT * FROM GpsPoints AS GpsPoint WHERE GpsPoint.trackerId = '1000107' AND (GpsPoint.date >= '2017-03-27 00:00:00.000 +00:00' AND GpsPoint.date <= '2017-03-28 00:00:00.000 +00:00') AND (GpsPoint.speed >= '0' AND GpsPoint.speed <= '1000') AND ST_Intersects(GpsPoint.point::geometry, ST_MakeEnvelope(9.855459, -87.129975, -52.419719, -31.363149, 4326));"
	)
	.then((rows) => {
		res.json(rows);
	}).catch((err) => {
		console.log('error al listar puntos', err);
	})
});

router.post('/', (req, res) => {
    var bodyData = req.body;
    var data = {
        trackerId: bodyData.trackerId,
        date: new Date(parseInt(bodyData.date)),
        point: {type: "Point", coordinates: [parseFloat(bodyData.latitude), parseFloat(bodyData.longitude)]},
        speed: parseFloat(bodyData.speed.replace(".",",")),
        altitude: bodyData.altitude,
        gpsValid: bodyData.gpsValid
    }
    console.log('velocidad', data.speed);
    models.GpsPoint.create(data, (err)=>{
        console.log('error al crear el punto', err)
    })
})

module.exports = router;