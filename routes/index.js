const models  = require('../models');
const express = require('express');
const router  = express.Router();

router.get('/', (req, res) => {
  models.User.findAll().then(function(users) {
    res.json(users);
  })
  .catch(function(err){
    console.log(err);
  });
});

module.exports = router;